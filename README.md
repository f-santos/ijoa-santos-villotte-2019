Using quadratic discriminant analysis for osteometric pair-matching of long bone antimeres: An evaluation on modern and archaeological samples
==============================================================================================================================================

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

**Authors**: Frédéric, Santos, Sébastien Villotte

*International Journal of Osteoarchaeology*, 2019.

**Supporting Information: R code.**

This set of R source files allows the replication of all the results (Tables and Figures) presented in the article.  All source files are independent (running all of them is not mandatory); and each Table or Figure is associated to an R source file.

Please note that :
* Rather than using `apply` or `lapply` R functions, various intermediate R objects are created on purpose in each R script, so that they can be directly accessed and inspected.
* The execution of some of the R scripts is time-consuming.
* The R package `bonepairs` must be installed. If not, please visit https://gitlab.com/f-santos/bonepairs
* The encoding system is UTF-8 for all source files.

The first draft of the manuscript (before any process of peer-review) can be found on the root of this repository.
