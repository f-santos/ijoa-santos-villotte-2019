### Using quadratic discriminant analysis for osteometric pair-matching of long bone antimeres:
### An evaluation on modern and archaeological samples.
### Authors: Frédéric, Santos, Sébastien Villotte.
### International Journal of Osteoarchaeology, 2019.
### Supporting Information: R code.
###
### Figure 1. Principal component analysis on right-left differences of all humeral measurements,
###           including the five samples under study.
###
### Correspondence to: Frédéric Santos, <frederic.santos@u-bordeaux.fr>

library(bonepairs) # Please make sure that this package is installed; otherwise visit https://gitlab.com/f.santos/bonepairs
library(FactoMineR) # Required for PCA. Please make sure that this package is installed.

#################################
### 1. Load and prepare dataset #
#################################
## Choose the samples that must be extracted from the Goldman Data Set:
samples <- c("Russia, Hamann-Todd", "Austria, Hamann-Todd", "Hungary, Hamann-Todd",
             "Poland, Hamann-Todd", "Ireland, Hamann-Todd", "Germany, Hamann-Todd",
             "Italy, Hamann-Todd", "Indian Knoll", "Poundbury (English Roman)",
             "Sayala", "Tigara - Point Hope, AK")
shortcodes <- c("Russia", "Austria", "Hungary", "Poland", "Ireland", "Germany", "Italy",
                "Indian Knoll", "Poundbury", "Sayala", "Tigara")
groups <- c(rep("Modern", 7), "Indian Knoll", "Poundbury", "Sayala", "Tigara")
## Load the dataset with the specified parameters:
liste <- get_GDS(bone = "humerus", summarize = FALSE, samples = samples,
                 shortcodes = shortcodes, groups = groups, rename = FALSE,
                 threshold.outliers = 4)
## Turn it into a dataframe (rather than a list):
dtf <- do.call("rbind", liste)
## Keep only the essential columns/variables for PCA:
dtf <- dtf[, -c(1:3)]
## Recode the levels of geographical origin
## (i.e., melt in one 'modern' sample all the contemporary population samples): 
levels(dtf$NOTE) <- c("Indian Knoll", rep("Modern", 7),
                      "Poundbury", "Sayala", "Tigara")
## Compute all right-left differences for humeral measurements:
head(dtf)
differences <- dtf[, c("RHML", "RHEB", "RHHD", "RHMLD", "RHAPD")] - dtf[, c("LHML", "LHEB", "LHHD", "LHMLD", "LHAPD")]
## Complete this dataframe with geographical origin and rename the columns: 
colnames(differences) <- substr(colnames(differences), 2, nchar(colnames(differences)))
differences <- data.frame(Orig = dtf$NOTE, differences)
## Summarize the dataset before the PCA:
summary(differences)

##########################
### 2. PCA for asymmetry #
##########################
## Choose a color palette for the PCA plot (should be ~~ suitable for colorblind users):
couleurs <- c("indianred", "gray", "navyblue", "goldenrod", "aquamarine2")
## Compute PCA on raw differences (exported in EPS format in the working directory):
setEPS()
postscript("Fig1_PCA_hum.eps", horizontal = FALSE, width = 7, height = 3.5)
## Split the graphical deice into two panels:
layout(matrix(c(1, 2), ncol = 2), widths = c(5, 3), heights = c(1, 1))
par(cex = 0.55)
## Compute PCA results:
res.pca <- PCA(differences, quali.sup = 1, graph = FALSE)
## Display the individual factor map:
plot(res.pca, habillage = 1, label = "none", invisible = "quali",
     palette = palette(couleurs), legend = list(cex = 1))
## Add group centroids:
points(res.pca$quali.sup$coord[, 1:2], pch = 8, cex = 1.6, col = couleurs)
## Display the correlation circle:
plot(res.pca, choix = "var")
dev.off()
