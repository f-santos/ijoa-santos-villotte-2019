### Using quadratic discriminant analysis for osteometric pair-matching of long bone antimeres:
### An evaluation on modern and archaeological samples.
### Authors: Frédéric, Santos, Sébastien Villotte.
### International Journal of Osteoarchaeology, 2019.
### Supporting Information: R code.
###
### Figure 4b. ROC analysis obtained for the three methods of pair-matching
###            on the modern sample using four femoral measurements.
### Warning: LOOCV calculations are time-consuming in this implementation.
###
### Correspondence to: Frédéric Santos, <frederic.santos@u-bordeaux.fr>

library(bonepairs) # Please make sure that this package is installed; otherwise visit https://gitlab.com/f.santos/bonepairs

###############################
# 1. Load and prepare dataset #
###############################
## 1.1. Load the dataset:
samples <- c("Russia, Hamann-Todd", "Austria, Hamann-Todd", "Hungary, Hamann-Todd",
             "Poland, Hamann-Todd", "Ireland, Hamann-Todd", "Germany, Hamann-Todd",
             "Italy, Hamann-Todd")
shortcodes <- c("Russia", "Austria", "Hungary", "Poland",
                "Ireland", "Germany", "Italy")
groups <- c(rep("Modern", length(samples)))
liste <- get_GDS(bone = "femur", summarize = FALSE, samples = samples,
                 shortcodes = shortcodes, groups = groups,
                 threshold.outliers = 4)
## 1.2. Set the training/learning dataset:
train <- liste$Modern
## Keep apart geographical information and remove nonmetric columns from the dataset:
trainOrig <- train$NOTE # NULL or train$NOTE
train <- train[, 5:ncol(train)]
head(train) # display the first 6 rows of the dataset
## Create two separate dataframes with a subset of measurements for left and right femora:
trainL <- train[, -grep("R", colnames(train))][, c(3,4,6,7)] # left femora
trainR <- train[, grep("R", colnames(train))][, c(3,4,6,7)] # right femora

#####################################################
### 2. ROC analysis using four femoral measurements #
#####################################################
## 2.1. Pair-matching using Byrd & Adams' (2003, 2006) model,
##      based on regressions:
res.logreg <- pm_logreg_LOO(learningL = trainL, learningR = trainR,
                            sitesLearning = trainOrig, graph = FALSE)
## 2.2. Pair-matching using Lynch's osteometric pair-matching model,
##      based on t-tests:
res.lynch <- pm_lynch_LOO(learningL = trainL, learningR = trainR,
                          sitesLearning = trainOrig, boxcox = TRUE,
                          abs = TRUE)
## 2.3. Pair-matching using QDA:
res.qda <- pm_qda_LOO(learningL = trainL, learningR = trainR,
                      sitesLearning = trainOrig)
## 2.4. ROC analysis:
setEPS() # export in EPS format in the working directory
postscript("Fig4b_ROC_fem_4mes.eps", horizontal = FALSE, width = 5, height = 5)
par(cex = 0.8)
pm_plot_ROC(classRes = list(res.lynch$indivResCV, res.logreg$indivResCV, res.qda$indivResCV),
            names = c("Lynch's t-tests", "Linear regressions", "QDA"),
            title = "Femur, modern sample (FEB, FAB, FMLD, FAPD)",
            plotlevel = c(0.01, 0.05, 0.1, 0.25, 0.5),
            best.method = NULL)
dev.off()
