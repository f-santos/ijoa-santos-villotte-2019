### Using quadratic discriminant analysis for osteometric pair-matching of long bone antimeres:
### An evaluation on modern and archaeological samples.
### Authors: Frédéric, Santos, Sébastien Villotte.
### International Journal of Osteoarchaeology, 2019.
### Supporting Information: R code.
###
### Table 6. Results of the three statistical methods for the pair-matching of left and right femora
###          using all measurements, given at an alpha level of 0.10.
### Warning: LOOCV calculations are time-consuming in this implementation.
###
### Correspondence to: Frédéric Santos, <frederic.santos@u-bordeaux.fr>

library(bonepairs) # Please make sure that this package is installed; otherwise visit https://gitlab.com/f.santos/bonepairs

#################################
### 1. Load and prepare dataset #
#################################
## 1.1. Load the dataset:
samples <- c("Russia, Hamann-Todd", "Austria, Hamann-Todd", "Hungary, Hamann-Todd",
             "Poland, Hamann-Todd", "Ireland, Hamann-Todd", "Germany, Hamann-Todd",
             "Italy, Hamann-Todd", "Indian Knoll", "Poundbury (English Roman)",
             "Sayala", "Tigara - Point Hope, AK")
shortcodes <- c("Russia", "Austria", "Hungary", "Poland",
                "Ireland", "Germany", "Italy",
                "Indian Knoll", "Poundbury", "Sayala", "Tigara")
groups <- c(rep("Modern", 7), "Indian Knoll", "Poundbury", "Sayala", "Tigara")
liste <- get_GDS(bone = "femur", summarize = FALSE, samples = samples,
                 shortcodes = shortcodes, groups = groups,
                 threshold.outliers = 4, rename = FALSE)

## 1.2. Set the training/learning (modern) dataset:
train <- liste$Modern
## Keep apart geographical information and remove nonmetric columns from the dataset:
trainOrig <- train$NOTE
train <- train[, 5:ncol(train)]
head(train) # display the first 6 rows of the dataset
## Create two separate dataframes for left and right femora:
trainL <- train[, -grep("R", colnames(train))] # left femur
trainR <- train[, grep("R", colnames(train))] # right femur

# 1.3. Set the test samples:
indiank <- liste$`Indian Knoll`[, -c(1:4)]
indiankL <- indiank[, -grep("R", colnames(indiank))] # left femur
indiankR <- indiank[, grep("R", colnames(indiank))] # right femur

pound <- liste$Poundbury[, -c(1:4)]
poundL <- pound[, -grep("R", colnames(pound))] # left femur
poundR <- pound[, grep("R", colnames(pound))] # right femur

sayala <- liste$Sayala[, -c(1:4)]
sayalaL <- sayala[, -grep("R", colnames(sayala))] # left femur
sayalaR <- sayala[, grep("R", colnames(sayala))] # right femur

tigara <- liste$Tigara[, -c(1:4)]
tigaraL <- tigara[, -grep("R", colnames(tigara))] # left femur
tigaraR <- tigara[, grep("R", colnames(tigara))] # right femur

#############################################
### 2. Initialize an empty table of results #
#############################################
Table <- matrix(NA, ncol = 10, nrow = 3)
rownames(Table) <- c("Lynch's t-tests", "Linear regressions", "QDA")
colnames(Table) <- paste(rep(c("Modern", "Indian Knoll", "Poundbury", "Sayala", "Tigara"), each = 2),
                         rep(c("TPR", "FPR"), times = 5),
                         sep="_")
Table

#####################################################
### 3. Pair-matching using all femoral measurements #
#####################################################
### 3.1. LOOCV on the modern sample
## 3.1.1. Pair-matching using Lynch's osteometric pair-matching model (t-tests):
res.lynch.loo.all <- pm_lynch_auto(learningL = trainL, learningR = trainR, sitesLearning = trainOrig,
                                   testL = NULL, testR = NULL, boxcox = TRUE, abs = TRUE, level = 0.1)
## 3.1.2. Pair-matching using Byrd & Adams' (2003, 2006) model based on regressions:
res.logreg.loo.all <- pm_logreg_auto(learningL = trainL, learningR = trainR, sitesLearning = trainOrig,
                                     testL = NULL, testR = NULL, graph = FALSE, level = 0.1)
## 3.1.3. Pair-matching using QDA:
res.qda.loo.all <- pm_qda_auto(learningL = trainL, learningR = trainR, sitesLearning = trainOrig,
                               testL = NULL, testR = NULL, level = 0.1)
## 3.1.4. Fill-in the Table:
Table["Lynch's t-tests", c("Modern_TPR", "Modern_FPR")] <- as.numeric(pm_results(res.lynch.loo.all$indivResCV)[2:3])
Table["Linear regressions", c("Modern_TPR", "Modern_FPR")] <- as.numeric(pm_results(res.logreg.loo.all$indivResCV)[2:3])
Table["QDA", c("Modern_TPR", "Modern_FPR")] <- as.numeric(pm_results(res.qda.loo.all$indivResCV)[2:3])
 
### 3.2. Indian Knoll sample
## 3.2.1. Pair-matching using Lynch's osteometric pair-matching model (t-tests):
res.lynch.ik.all <- pm_lynch_auto(learningL = trainL, learningR = trainR, sitesLearning = trainOrig,
                                  testL = indiankL, testR = indiankR, boxcox = TRUE, abs = TRUE, level = 0.1)
## 3.2.2. Pair-matching using Byrd & Adams' (2003, 2006) model based on regressions:
res.logreg.ik.all <- pm_logreg_auto(learningL = trainL, learningR = trainR, sitesLearning = trainOrig,
                                    testL = indiankL, testR = indiankR, graph = FALSE, level = 0.1)
## 3.2.3. Pair-matching using QDA:
res.qda.ik.all <- pm_qda_auto(learningL = trainL, learningR = trainR, sitesLearning = trainOrig,
                              testL = indiankL, testR = indiankR, level = 0.1)
## 3.2.4. Fill-in the Table:
Table["Lynch's t-tests", c("Indian Knoll_TPR", "Indian Knoll_FPR")] <- as.numeric(pm_results(res.lynch.ik.all$indivResTest)[2:3])
Table["Linear regressions", c("Indian Knoll_TPR", "Indian Knoll_FPR")] <- as.numeric(pm_results(res.logreg.ik.all$indivResTest)[2:3])
Table["QDA", c("Indian Knoll_TPR", "Indian Knoll_FPR")] <- as.numeric(pm_results(res.qda.ik.all$indivResTest)[2:3])

### 3.3. Poundbury sample
## 3.3.1. Pair-matching using Lynch's osteometric pair-matching model (t-tests):
res.lynch.po.all <- pm_lynch_auto(learningL = trainL, learningR = trainR, sitesLearning = trainOrig,
                                  testL = poundL, testR = poundR, boxcox = TRUE, abs = TRUE, level = 0.1)
## 3.3.2. Pair-matching using Byrd & Adams' (2003, 2006) model based on regressions:
res.logreg.po.all <- pm_logreg_auto(learningL = trainL, learningR = trainR, sitesLearning = trainOrig,
                                    testL = poundL, testR = poundR, graph = FALSE, level = 0.1)
## 3.3.3. Pair-matching using QDA:
res.qda.po.all <- pm_qda_auto(learningL = trainL, learningR = trainR, sitesLearning = trainOrig,
                              testL = poundL, testR = poundR, level = 0.1)
## 3.3.4. Fill-in the Table:
Table["Lynch's t-tests", c("Poundbury_TPR", "Poundbury_FPR")] <- as.numeric(pm_results(res.lynch.po.all$indivResTest)[2:3])
Table["Linear regressions", c("Poundbury_TPR", "Poundbury_FPR")] <- as.numeric(pm_results(res.logreg.po.all$indivResTest)[2:3])
Table["QDA", c("Poundbury_TPR", "Poundbury_FPR")] <- as.numeric(pm_results(res.qda.po.all$indivResTest)[2:3])
 
### 3.4. Sayala sample
## 3.4.1. Pair-matching using Lynch's osteometric pair-matching model (t-tests):
res.lynch.sa.all <- pm_lynch_auto(learningL = trainL, learningR = trainR, sitesLearning = trainOrig,
                                  testL = sayalaL, testR = sayalaR, boxcox = TRUE, abs = TRUE, level = 0.1)
## 3.4.2. Pair-matching using Byrd & Adams' (2003, 2006) model based on regressions:
res.logreg.sa.all <- pm_logreg_auto(learningL = trainL, learningR = trainR, sitesLearning = trainOrig,
                                    testL = sayalaL, testR = sayalaR, graph = FALSE, level = 0.1)
## 3.4.3. Pair-matching using QDA:
res.qda.sa.all <- pm_qda_auto(learningL = trainL, learningR = trainR, sitesLearning = trainOrig,
                              testL = sayalaL, testR = sayalaR, level = 0.1)
## 3.4.4. Fill-in the Table:
Table["Lynch's t-tests", c("Sayala_TPR", "Sayala_FPR")] <- as.numeric(pm_results(res.lynch.sa.all$indivResTest)[2:3])
Table["Linear regressions", c("Sayala_TPR", "Sayala_FPR")] <- as.numeric(pm_results(res.logreg.sa.all$indivResTest)[2:3])
Table["QDA", c("Sayala_TPR", "Sayala_FPR")] <- as.numeric(pm_results(res.qda.sa.all$indivResTest)[2:3])

### 3.5. Tigara sample
## 3.5.1. Pair-matching using Lynch's osteometric pair-matching model (t-tests):
res.lynch.ti.all <- pm_lynch_auto(learningL = trainL, learningR = trainR, sitesLearning = trainOrig,
                                  testL = tigaraL, testR = tigaraR, boxcox = TRUE, abs = TRUE, level = 0.1)
## 3.5.2. Pair-matching using Byrd & Adams' (2003, 2006) model based on regressions:
res.logreg.ti.all <- pm_logreg_auto(learningL = trainL, learningR = trainR, sitesLearning = trainOrig,
                                    testL = tigaraL, testR = tigaraR, graph = FALSE, level = 0.1)
## 3.5.3. Pair-matching using QDA:
res.qda.ti.all <- pm_qda_auto(learningL = trainL, learningR = trainR, sitesLearning = trainOrig,
                              testL = tigaraL, testR = tigaraR, level = 0.1)
## 3.5.4. Fill-in the Table:
Table["Lynch's t-tests", c("Tigara_TPR", "Tigara_FPR")] <- as.numeric(pm_results(res.lynch.ti.all$indivResTest)[2:3])
Table["Linear regressions", c("Tigara_TPR", "Tigara_FPR")] <- as.numeric(pm_results(res.logreg.ti.all$indivResTest)[2:3])
Table["QDA", c("Tigara_TPR", "Tigara_FPR")] <- as.numeric(pm_results(res.qda.ti.all$indivResTest)[2:3])

########################
# 4. Display the table #
########################
Table <- round(100*Table, 1)
Table

# Export the table in LaTeX format:
library(Hmisc)
Table <- paste(Table, "%")
Table <- matrix(Table, ncol = 10)
rownames(Table) <- c("Lynch's t-tests", "Linear regressions", "QDA")
colnames(Table) <- rep(c("TPR", "FPR"), 5)
latex(Table,
      cgroup = c("Modern", "Indian Knoll (n=53)", "Poundbury (n=47)", "Sayala (n=24)", "Tigara (n=44)"),
      n.cgroup = rep(2, 5),
      colnamesTexCmd = "bfseries", first.hline.double = FALSE,
      numeric.dollar = FALSE,
      rowlabel = "Method",
      file = "", ctable = FALSE, star = TRUE, where = "h",
      center = "centering",
      caption = "Results of the three statistical methods for the pair-matching of left and right femora. The modern sample was used as the learning dataset, and the models were then applied on the archaeological samples. Classification accuracy is expressed by TPR, true positive rate, and FPR, false positive rate. All five available measurements were used. The results are given at an \alpha-level of 0.10.",
      label = "tab:HumResultsAt01AllMes"
     )
